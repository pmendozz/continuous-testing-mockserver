package model;

import org.mockserver.matchers.Times;

public class MockService {
    private String method;
    private String relativePath;
    private String fileRequest;
    private String fileResponse;
    private Times times;
    private int delay;

    public MockService(Times times, String method, String relativePath, String fileRequest, String fileResponse) {
        this.times = times;
        this.method = method;
        this.relativePath = relativePath;
        this.fileRequest = fileRequest;
        this.fileResponse = fileResponse;
        this.delay = 1;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public String getFileRequest() {
        return fileRequest;
    }

    public void setFileRequest(String fileRequest) {
        this.fileRequest = fileRequest;
    }

    public String getFileResponse() {
        return fileResponse;
    }

    public void setFileResponse(String fileResponse) {
        this.fileResponse = fileResponse;
    }

    public Times getTimes() {
        return times;
    }

    public void setTimes(Times times) {
        this.times = times;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}

