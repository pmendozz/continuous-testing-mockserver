package flows;

import model.MockService;

import java.util.ArrayList;
import java.util.List;

import static org.mockserver.matchers.Times.unlimited;

public class FlowLogin {

    public static ArrayList<MockService> build(String basePath) {
        List<MockService> flow_sequence = new ArrayList<MockService>();

        flow_sequence.add(new MockService(unlimited(), "POST", basePath + "/login"
                , "/xmlFileRequest/op_login_I22P40000000.xml"
                , "/xmlFileResponse/op_login_I22P40000000.xml"));
        flow_sequence.add(new MockService(unlimited(), "POST", basePath + "/login"
                , "/xmlFileRequest/op_login_I36P36000000.xml"
                , "/xmlFileResponse/op_login_I36P36000000.xml"));

        return (ArrayList<MockService>) flow_sequence;

    }
}
