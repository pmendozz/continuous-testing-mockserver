package runmockserver;


import flows.FlowLogin;
import model.MockService;
import org.apache.commons.io.IOUtils;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.Times;
import org.mockserver.model.Delay;
import org.mockserver.model.Header;
import util.Parameters;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


public class RunMockServer {

    public static void main(String[] args) throws IOException {

        MockServerClient mockServerClient = new MockServerClient("127.0.0.1", 1080);

        mockServerClient.reset();

        List<MockService> currentFlow;

        currentFlow = new ArrayList<MockService>(FlowLogin.build(Parameters.basePath));


        for (MockService mockService : currentFlow) {
            registerGenericService(mockServerClient,
                    mockService.getTimes(),
                    mockService.getMethod(),
                    mockService.getRelativePath(),
                    mockService.getFileRequest(),
                    mockService.getFileResponse(),
                    mockService.getDelay());
        }

    }

    private static void registerGenericService(MockServerClient client, Times times,
                                               String method, String relativePath,
                                               String fileRequest, String fileResponse,
                                               int delay)
            throws IOException {

        InputStream isFileRequest = RunMockServer.class.getResourceAsStream(fileRequest);
        InputStream isFileResponse = RunMockServer.class.getResourceAsStream(fileResponse);

        String fileRequestString = null;
        if (isFileRequest != null) {
            fileRequestString = IOUtils.toString(isFileRequest);
        } else {
            System.console().printf("isFileRequest: null");
        }

        String fileResponseString = null;
        if (isFileResponse != null) {
            fileResponseString = IOUtils.toString(isFileResponse);
        } else {
            System.console().printf("isFileResponse: null");
        }

        client
                .when(
                        request()
                                .withMethod(method)
                                .withPath(relativePath)
                                .withBody(fileRequestString)
                        ,
                        times
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8")
                                )
                                .withBody(fileResponseString)
                                .withDelay(new Delay(TimeUnit.SECONDS, delay))
                );
    }
}